
#
# Exemple de fichier Makefile pour lancer le formattage
# du fichier presentation.tex
#

CIBLES = presentation.pdf

PDFLATEX = pdflatex

all:	$(CIBLES)

# 3 passages de latex pour assurer une coherence de la
# bibliographie
presentation.pdf: presentation.tex *.sty img/*
	$(PDFLATEX) presentation.tex
	$(PDFLATEX) presentation.tex

clean:
	rm -f $(CIBLES) *.aux *.log *.blg *.bbl *.out *.toc *.nav *.snm
